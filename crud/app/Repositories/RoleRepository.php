<?php

namespace App\Repositories;

use App\Models\Role;
use Illuminate\Support\Facades\DB;

class RoleRepository extends BaseRepository
{
    public function model()
    {
        return Role::class;
    }

    public function search($dataSearch)
    {
        return $this->model->withName($dataSearch['role_name'])->latest()->paginate(5);
    }

    public function createRoleHasPermissions($permissions, $idRole)
    {
        $this->model->createRoleHasPermissions($permissions, $idRole);
    }

    public function updateRoleHasPermissions($permissions, $idRole)
    {
        $this->model->updateRoleHasPermissions($permissions, $idRole);
    }
}

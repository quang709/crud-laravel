<?php

namespace App\Repositories;

use App\Models\Role;
use App\Models\User;
use Illuminate\Support\Facades\DB;

class UserRepository extends BaseRepository
{

    public function model()
    {
        return User::class;
    }

    public function createUserHasRoles($id, $idRole)
    {
        $this->model->createUserHasRoles($id, $idRole);
    }

    public function updateUserHasRoles($id, $idRole)
    {
        $this->model->updateUserHasRoles($id, $idRole);
    }

    public function createUserHasRolesIsNull($user)
    {
        $this->model->createUserHasRolesIsNull($user);
    }

    public function search($dataSearch)
    {
        return $this->model->withRoleName($dataSearch['role_name'])->withName($dataSearch['name'])
            ->withEmail($dataSearch['email'])->latest()->paginate(5);
    }
}

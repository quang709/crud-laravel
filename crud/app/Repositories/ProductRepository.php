<?php

namespace App\Repositories;

use App\Models\Product;
use Illuminate\Support\Facades\DB;

class ProductRepository extends BaseRepository
{
    public function model()
    {
        return product::class;
    }

    public function createCategoryHasProducts($id, $idProduct)
    {
        $this->model->createCategoryHasProducts($id, $idProduct);
    }

    public function updateCategoryHasProducts($id, $idProducts)
    {
        $this->model->updateCategoryHasProducts($id, $idProducts);
    }

    public function search($dataSearch)
    {
        return $this->model->withName($dataSearch['name'])->withSummary($dataSearch['summary'])
            ->withCategoryName($dataSearch['category_name'])->latest()->paginate(5);
    }
}

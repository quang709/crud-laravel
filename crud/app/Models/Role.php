<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Role extends Model
{
    use HasFactory;
    protected $table = "roles";
    protected $fillable = ['name'];

    public function user()
    {
        return $this->belongsToMany(User::class, 'user_has_roles');
    }

    public function permissions()
    {
        return $this->belongsToMany(Permission::class, 'role_has_permissions');
    }

    public function scopeWithName($query, $role_name)
    {
        return $query->where('name', 'LIKE', '%' . $role_name . '%');
    }

    public function hasPermission($permissionName)
    {
        foreach ($this->permissions as $permission) {
            if ($permission->name == $permissionName) {
                return true;
            }
        }
        return false;
    }

    public function createRoleHasPermissions($permissions, $idRole)
    {
        foreach ($permissions as $value) {
            DB::table('role_has_permissions')->insert([
                'permission_id' => $value,
                'role_id' => $idRole,
            ]);
        }
    }

    public function updateRoleHasPermissions($permissions, $idRole)
    {
        DB::table('role_has_permissions')->where('role_id', $idRole)->delete();
        foreach ($permissions as $value) {
            DB::table('role_has_permissions')->insert([
                'permission_id' => $value,
                'role_id' => $idRole,
            ]);
        }
    }
}

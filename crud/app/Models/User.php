<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\DB;
use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'email',
        'password',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];


    public function roles()
    {
        return $this->belongsToMany(Role::class, 'user_has_roles');
    }

    /**
     * Scope a query to search user by first name.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeWithName($query, $Name)
    {
        return  $query->where('name', 'LIKE', '%' . $Name . '%') ;
    }

    /**
     * Scope a query to search user by email.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeWithEmail($query, $email)
    {
        return  $query->where('email', 'LIKE', '%' . $email . '%');
    }

    /**
     * Scope a query to search user by role.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeWithRoleName($query, $roleName)
    {
        return  $query->whereHas(
            'roles',
            fn($query) => $query->where('name', 'LIKE', '%' . $roleName . '%')
        );
    }

    public function hasRole($roleName)
    {
        foreach ($this->roles as $role) {
            if ($role->name == $roleName) {
                return true;
            }
        }
        return false;
    }

    public function hasPermission($permissionName)
    {
        $listRole = $this->roles;
        foreach ($listRole as $role) {
            if ($role->hasPermission($permissionName)) {
                return true;
            }
        }
        return false;
    }

    public function createUserHasRoles($id, $idRole)
    {
        DB::table('user_has_roles')->insert([
            'role_id' => $idRole,
            'user_id' => $id,
        ]);
    }

    public function updateUserHasRoles($id, $idRole)
    {
        DB::table('user_has_roles')->where('user_id', $id)->delete();
        DB::table('user_has_roles')->insert([
            'user_id' => $id,
            'role_id' => $idRole
        ]);
    }

    public function createUserHasRolesIsNull($user)
    {
        $role = Role::where('name', 'new member')->first();
        $user->roles()->attach($role->id);
    }
}

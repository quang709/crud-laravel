<?php

namespace App\Services;

use App\Repositories\UserRepository;
use Illuminate\Support\Facades\Hash;

class UserService
{
    /**
     * @var $userRepository
     */
    protected $userRepository;

    /**
     * PostService constructor.
     *
     * @param UserRepository $userRepository ;
     */
    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    public function search($request)
    {
        $dataSearch['name'] = $request->name;
        $dataSearch['role_name'] = $request->role_name;
        $dataSearch['email'] = $request->email;
        return $this->userRepository->search($dataSearch);
    }

    public function create($request)
    {
        $data['name'] = $request->name;
        $data['email'] = $request->email;
        $data['password'] = Hash::make($request->password);
        $user = $this->userRepository->create($data);
        if (isset($request->role)) {
            $idRole = $request->role;
            $this->userRepository->createUserHasRoles($user->id, $idRole);
        } else {
            $this->userRepository->createUserHasRolesIsNull($user);
        }
    }

    public function findOrFail($id)
    {
        return $this->userRepository->findOrFail($id);
    }

    public function update($request, $id)
    {
        $data['name'] = $request->name;
        if (isset($request->password)) {
            $data['password'] = Hash::make($request->password);
        }
        $user = $this->userRepository->update($data, $id);
        if (isset($request->role)) {
            $idRole = $request->role;
            $this->userRepository->updateUserHasRoles($user->id, $idRole);
        }
    }

    public function delete($id)
    {
        return $this->userRepository->delete($id);
    }
}

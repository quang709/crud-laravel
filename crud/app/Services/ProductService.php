<?php

namespace App\Services;

use App\Models\Category_has_products;
use App\Models\User_has_Role;
use App\Repositories\ProductRepository;
use App\Traits\HandleImage;
use Illuminate\Support\Str;
use Image;
use Redirect;
use Response;

class ProductService
{
    use HandleImage;

    /**
     * @var $ProductRepository
     */
    protected $productRepository;


    /**
     * PostService constructor.
     *
     * @param ProductRepository $productRepository ;
     */
    public function __construct(ProductRepository $productRepository)
    {
        $this->productRepository = $productRepository;
    }

    public function search($request)
    {
        $dataSearch['name'] = $request->name;
        $dataSearch['category_name'] = $request->category_name;
        $dataSearch['summary'] = $request->summary;
        return $this->productRepository->search($dataSearch);
    }

    public function create($request)
    {
        $data = $request->all();
        $data['slug'] = Str::slug($request->name);
        $data['image'] = $this->storeImage($request);
        $product = $this->productRepository->create($data);
        $this->productRepository->createCategoryHasProducts($request->parent_id, $product->id);
    }

    public function findOrFail($id)
    {
        return $this->productRepository->findOrFail($id);
    }

    public function update($request, $id)
    {
        if ($request->image == null) {
            unset($request->image);
            $data['name'] = $request->name;
            $data['slug'] = Str::slug($request->name);
            $data['summary'] = $request->summary;
            $data['content'] = $request->content;
            $product = $this->productRepository->update($data, $id);
        } else {
            $product = $this->productRepository->findOrFail($id);
            $data = $request->all();
            $data['slug'] = Str::slug($request->name);
            $data['image'] = $this->updateImage($request, $product->image);
            $product = $this->productRepository->update($data, $id);
        }
        if ($request->parent_id == 0) {
            unset($request->parent_id);
        } else {
            $this->productRepository->updateCategoryHasProducts($request->parent_id, $product->id);
        }
    }

    public function delete($id)
    {
        $product = $this->productRepository->find($id);
        $this->deleteImage($product->image);
        return $this->productRepository->delete($id);
    }
}

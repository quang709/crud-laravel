<?php

namespace App\Services;

use App\Models\Category_has_products;
use App\Models\User_has_Role;
use App\Repositories\CategoryRepository;
use Illuminate\Support\Str;

class CategoryService
{
    /**
     * @var $CategoryRepository
     */
    protected $categoryRepository;

    /**
     * PostService constructor.
     *
     * @param CategoryRepository $categoryRepository ;
     */
    public function __construct(CategoryRepository $categoryRepository)
    {
        $this->categoryRepository = $categoryRepository;
    }

    public function all()
    {
        return $this->categoryRepository->all();
    }

    public function search($request)
    {
        $dataSearch['category_name'] = $request->category_name;
        return $this->categoryRepository->search($dataSearch);
    }

    public function getCategoryParent()
    {
        return $this->categoryRepository->getCategoryParent();
    }

    public function create($request)
    {
        $data['name'] = $request->name;
        $data['slug'] = Str::slug($request->name);
        $category = $this->categoryRepository->create($data);
        $parentId = $request->parent_id;
        $this->categoryRepository->createParent($category->id, $parentId);
    }

    public function findOrFail($id)
    {
        return $this->categoryRepository->findOrFail($id);
    }

    public function update($request, $id)
    {
        $data['name'] = $request->name;
        $data['slug'] = Str::slug($request->name);
        $category = $this->categoryRepository->update($data, $id);
        if ($category->id == $request->parent_id) {
            unset($request->parent_id);
        } else {
            $parentId = $request->parent_id;
            $this->categoryRepository->updateParent($category->id, $parentId);
        }
    }

    public function delete($id)
    {
        return $this->categoryRepository->delete($id);
    }
}

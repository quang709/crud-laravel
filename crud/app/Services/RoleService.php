<?php

namespace App\Services;

use App\Models\Permission;
use App\Models\Role;
use App\Models\Role_has_Permission;
use App\Repositories\RoleRepository;
use Exception;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use InvalidArgumentException;

class RoleService
{
    /**
     * @var $roleRepository
     */
    protected $roleRepository;

    /**
     * PostService constructor.
     *
     * @param RoleRepository $roleRepository ;
     */
    public function __construct(RoleRepository $roleRepository)
    {
        $this->roleRepository = $roleRepository;
    }

    /**
     * Get All post
     * @return String
     */
    public function search($request)
    {

        $dataSearch['role_name'] = $request->role_name;
        return $this->roleRepository->search($dataSearch);
    }

    public function all()
    {
        return $this->roleRepository->all();
    }

    public function findOrFail($id)
    {
        return $this->roleRepository->findOrFail($id);
    }

    public function create($request)
    {
        $data_input['name'] = $request->name;
        $role = $this->roleRepository->create($data_input);
        $this->roleRepository->createRoleHasPermissions($request->permission, $role->id);
    }

    public function update($request, $id)
    {
        $data['name'] = $request->name;
        $role = $this->roleRepository->update($data, $id);
        $this->roleRepository->updateRoleHasPermissions($request->permission, $role->id);
    }

    public function delete($id)
    {
        $this->roleRepository->delete($id);
    }
}

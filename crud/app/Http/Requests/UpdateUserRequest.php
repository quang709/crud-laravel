<?php

namespace App\Http\Requests;

use App\Rules\UppercaseRule;
use Illuminate\Foundation\Http\FormRequest;

class UpdateUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if (isset($this->changePassword)) {
            return [

                'name' => ['required'],
                'password' => ['required', 'min:3'],
                'password_confirmation' => ['required', 'min:3', 'same:password'],
            ];
        } else {
            return [

                'name' => ['required'],

            ];
        }
    }
}

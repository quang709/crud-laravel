<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreProductRequest;
use App\Http\Requests\UpdateProductRequest;
use Image;
use App\Models\Category;
use App\Models\Product;
use App\Models\Role;
use Illuminate\Http\Request;
use App\Services\CategoryService;
use App\Services\ProductService;

class ProductController extends Controller
{

    protected $productService;
    protected $categoryService;

    public function __construct(ProductService $productService, CategoryService $categoryService)
    {
        $this->productService = $productService;
        $this->categoryService = $categoryService;
    }

    public function index(Request $request)
    {
        $products = $this->productService->search($request);
        $category = $this->categoryService->all();
        $parent = $this->categoryService->getCategoryParent();
        return view('products.index', ['products' => $products, 'parent' => $parent, 'category' => $category]);
    }


    public function store(StoreProductRequest $request)
    {
        $this->productService->create($request);
        return redirect(route('products.index'));
    }

    public function edit($id)
    {

        $productEdit = $this->productService->findOrFail($id);

        return response()->json(['productEdit' => $productEdit]);
    }

    public function update(UpdateProductRequest $request, $id)
    {
        $this->productService->update($request, $id);
        return redirect(route('products.index'));
    }

    public function destroy($id)
    {
        $this->productService->delete($id);
        return redirect(route('products.index'));
    }
}

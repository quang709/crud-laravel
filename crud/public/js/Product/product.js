$(function () {
    $(".js-modal-create").click(function (event) {
        event.preventDefault();
        $("#myModal").modal('show');
        $("#form-create")[0].reset();

    })
    $(".js-modal-update").click(function (event) {
        event.preventDefault();

        let url = $(this).data('route-edit');

        $.ajax({
            url: url,
            method: "GET",
            dataType: 'json',
            success: function (response) {
                // console.log(response);
                $('#name').val(response.productEdit.name);
                $('#summary').val(response.productEdit.summary);
                $('#content').val(response.productEdit.content);
                $('#id').val(response.productEdit.id);

            }
        })
        $("#myModalUpdate").modal('show');
        $("#form-update")[0].reset();


    });
    $(".js-modal-delete").click(function (event) {
        event.preventDefault();
        $("#myModalDelete").modal('show');
        let url = $(this).data('route');


        $('.js-btn-delete').click(function (e) {
            e.preventDefault();

            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: url,
                type: 'post',
                method: "post",
                success: function () {
                    $("#ms").html("<p> product has been delete!</p>");
                    window.setTimeout(function () {
                        location.reload()
                    }, 3000)
                }

            })


                .done(function (results) {
                    $("#myModalDelete").modal('hide');


                })

                .fail(function (data) {
                    var errors = data.responseJSON;
                    $.each(errors.errors, function (i, val) {
                        $domForm.find('input[name=' + i + ']').siblings('.error-form').text(val[0]);
                    });
                });
        });
    })


})

$(".close").click(function (event) {
    event.preventDefault();
    $("#myModal").modal('hide');
    $("#myModalUpdate").modal('hide');
    location.reload();
})
$("#xx").click(function (event) {
    event.preventDefault();
    $("#myModalDelete").modal('hide');


})


$('.js-btn-create').click(function (e) {
    e.preventDefault();
    let url = $("#form-create").data('route');
    var data = new FormData(document.getElementById("form-create"));

    let $this = $(this);
    let $domForm = $this.closest('form');
    $.ajax({

        type: 'POST',

        url: url,

        data: data,

        cache: false,

        contentType: false,

        processData: false,
    })

        .done(function (results) {
            $("#myModal").modal('hide');
            $("#form-create")[0].reset();
            $("#ms").html("<p> product has been create!</p>");
            window.setTimeout(function () {
                location.reload()
            }, 3000)

        })
        .fail(function (data) {
            var errors = data.responseJSON;
            $.each(errors.errors, function (i, val) {
                $domForm.find('input[name=' + i + ']').siblings('.error-form').text(val[0]);
            });
        });


});

$('.js-btn-update').click(function (e) {
    e.preventDefault();

    let $this = $(this);
    let $domForm = $this.closest('form');
    let url = $('#form-update').data('route-update');
    var data = new FormData(document.getElementById("form-update"));

    $.ajax({

        type: 'POST',

        url: url,

        data: data,

        cache: false,

        contentType: false,

        processData: false,
    })

        .done(function (results) {
            $("#myModalUpdate").modal('hide');
            $("#form-update")[0].reset();
            $("#ms").html("<p> products has been update!</p>");
            window.setTimeout(function () {
                location.reload()
            }, 3000)
        })
        .fail(function (data) {
            var errors = data.responseJSON;
            $.each(errors.errors, function (i, val) {
                $domForm.find('input[name=' + i + ']').siblings('.error-form').text(val[0]);
            });
        });


});

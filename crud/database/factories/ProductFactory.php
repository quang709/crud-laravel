<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class ProductFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $this->faker->name,
            'slug' => $this->faker->name,
            'summary' => $this->faker->name,
            'content' => $this->faker->text,
            'image' => $this->faker->image('public/upload', 400, 300, null, false),
        ];
    }
}

<?php

namespace Tests\Feature\Category;

use App\Models\Category;
use Illuminate\Http\Response;
use Tests\TestCase;

class ListCategoryTest extends TestCase
{

    public function getListCategoryRoute()
    {
        return route('categories.index');
    }

    /**
     * @test
     */
    public function authenticate_can_get_list_category()
    {
        $this->login(["admin"]);
        $response = $this->get($this->getListCategoryRoute());
        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('categories.index');
    }

    /**
     * @test
     */
    public function unauthenticated_can_get_list_category()
    {
        $category = Category::factory()->create();
        $response = $this->get($this->getListCategoryRoute());
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect('/login');
    }
}

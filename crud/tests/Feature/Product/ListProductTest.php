<?php

namespace Tests\Feature\Product;

use App\Models\Product;
use Illuminate\Http\Response;
use Tests\TestCase;

class ListProductTest extends TestCase
{
    public function getListProductRoute()
    {
        return route('products.index');
    }

    /**
     * @test
     */
    public function authenticate_can_get_list_product()
    {
        $this->login(["admin"]);
        $response = $this->get($this->getListProductRoute());
        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('products.index');
    }

    /**
     * @test
     */
    public function unauthenticated_can_not_get_list_role()
    {
        $user = Product::factory()->create();
        $response = $this->get($this->getListProductRoute());
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect('/login');
    }
}

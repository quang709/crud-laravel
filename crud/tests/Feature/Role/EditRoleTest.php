<?php

namespace Tests\Feature\Role;

use App\Models\Role;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Http\Response;
use Tests\TestCase;

class EditRoleTest extends TestCase
{
    use WithoutMiddleware;

    public function getEditRoleRoute($id)
    {
        return route('roles.update', $id);
    }

    public function getRedirectRoleRoute()
    {
        return route('roles.index');
    }

    /**
     * @test
     */

    public function authenticate_can_update_role()
    {
        $this->login(["admin"]);
        $role = Role::factory()->create();
        $data = ['name' => $this->faker->name(), 'permission' => array(1, 2, 3)];
        $response = $this->post($this->getEditRoleRoute($role->id), $data);
        $roleCheck = Role::find($role->id);
        $this->assertSame($roleCheck->name, $data['name']);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect($this->getRedirectRoleRoute());
    }

    /**
     * @test
     */

    public function authenticate_can_not_update_role_if_name_is_null()
    {
        $this->login(["admin"]);
        $role = Role::factory()->create();
        $data = ['name' => '', 'permission' => array(1, 2, 3)];
        $response = $this->post($this->getEditRoleRoute($role->id), $data);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertSessionHasErrors(['name']);
    }

    /**
     * @test
     */
    public function authenticate_can_not_update_role_if_permissison_is_null()
    {
        $this->login(["admin"]);
        $role = Role::factory()->create();
        $data = ['name' => $this->faker->name(), 'permission' => ''];
        $response = $this->post($this->getEditRoleRoute($role->id), $data);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertSessionHasErrors(['permission']);
    }


}

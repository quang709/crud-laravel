<?php

namespace Tests\Feature\User;

use App\Models\User;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Http\Response;
use Tests\TestCase;

class CreateUserTest extends TestCase
{

    use WithoutMiddleware;

    public function getStoreUserRoute()
    {
        return route('users.store');
    }

    public function getRedirectUserRoute()
    {
        return route('users.index');
    }

    /** @test */

    public function authenticate_can_store_user_if_login_is_admin()
    {

        $this->login(["admin"]);
        $data = [
            'name' => $this->faker->name(),
            'email' => $this->faker->unique()->safeEmail(),
            'password' => '123456', // password
        ];
        $userAfter = User::count();
        $response = $this->post($this->getStoreUserRoute(), $data);
        $userBefore = User::count();
        $this->assertEquals($userAfter + 1, $userBefore);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect($this->getRedirectUserRoute());
    }

    /** @test */

    public function authenticate_can_store_user_if_login_is_super_admin()
    {
        $this->login(["super-admin"]);
        $data = [
            'name' => $this->faker->name(),
            'email' => $this->faker->unique()->safeEmail(),
            'password' => '123456', // password
            'role' => 2
        ];
        $userAfter = User::count();
        $response = $this->post($this->getStoreUserRoute(), $data);
        $userBefore = User::count();
        $this->assertEquals($userAfter + 1, $userBefore);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect($this->getRedirectUserRoute());
    }

    /** @test */
    public function authenticate_can_not_store_user_if_login_is_admin_and_email_is_null()
    {
        $this->login(["admin"]);
        $data = [
            'name' => $this->faker->name(),
            'email' => '',
            'password' => '123456', // password

        ];
        $response = $this->post($this->getStoreUserRoute(), $data);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertSessionHasErrors(['email']);

    }

    /** @test */
    public function authenticate_can_not_store_user_if_login_is_admin_and_password_is_null()
    {
        $this->login(["admin"]);
        $data = [
            'name' => $this->faker->name(),
            'email' => $this->faker->unique()->safeEmail(),
            'password' => '', // password

        ];
        $response = $this->post($this->getStoreUserRoute(), $data);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertSessionHasErrors(['password']);

    }

    /** @test */
    public function authenticate_can_not_store_user_if_login_is_admin_and_name_is_null()
    {
        $this->login(["admin"]);
        $data = [
            'name' => '',
            'email' => $this->faker->unique()->safeEmail(),
            'password' => '123456', // password

        ];
        $response = $this->post($this->getStoreUserRoute(), $data);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertSessionHasErrors(['name']);

    }


}

<?php

namespace Tests\Feature\User;

use App\Models\User;
use Illuminate\Http\Response;
use Tests\TestCase;


class ListUserTest extends TestCase
{

    public function getListUserRoute()
    {
        return route('users.index');
    }

    /**
     * @test
     */
    public function anthenticate_can_get_list_user()
    {
        $this->login(["admin"]);
        $user = User::factory()->create();
        $response = $this->get($this->getListUserRoute());
        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('users.index');


    }

    /**
     * @test
     */
    public function unanthenticate_can_get_list_user()
    {
        $user = User::factory()->create();
        $response = $this->get($this->getListUserRoute());
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect('/login');

    }


}
